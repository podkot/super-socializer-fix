<?php
/*
Plugin Name: Super Socializer Share Icons Fix
Plugin URI: http://super-socializer-wordpress.heateor.com
Description: Inserting svgs into html. Look also bug https://wordpress.org/support/topic/social-icons-in-wrong-colors/?replies=4
Version: 1.1
License: GPL2+
*/
add_action('wp_enqueue_scripts', function(){
    if ( wp_style_is( 'the_champ_sharing_svg' ) ) {
        wp_deregister_style('the_champ_sharing_svg');
        wp_dequeue_style('the_champ_sharing_svg');
        wp_enqueue_style( 'the_champ_sharing_svg_fixed', plugins_url( 'share-default-svg-horizontal.css', __FILE__ ), false, '7.6' );
    }
}, 100000);

add_action('wp_print_footer_scripts', function(){
    ?>
    <script>
    jQuery(function($){
        setTimeout(function(){
            $('.the_champ_sharing_ul ss, .the_champ_login_ul ss').each(function(){
                var bg = $(this).css('background') || $(this).css('background-image'),
                    svg,
                    base64RE = '^.+?data:image\\/svg\\+xml;base64,',
                    utf8RE   = '^.+?data:image\\/svg\\+xml;charset=utf8,',
                    svgRE    = '([^)"\']+)["\']?\\s*\\).*$',
                    baseSvg;

                if ( new RegExp( base64RE, 'mi' ).test( bg ) ) {
                    if ( !window.atob ) {
                        polyfill_atob();
                    }

                    baseSvg = bg.replace( new RegExp( base64RE + svgRE, 'mi' ), '$1' );

                    if ( window.atob ) {
                        svg = window.atob( baseSvg );
                    }
                    else {
                        return;
                    }
                }
                else if ( new RegExp( utf8RE, 'mi' ).test( bg ) ) {
                    baseSvg = bg.replace( new RegExp( utf8RE + svgRE, 'mi' ), '$1');
                    svg = decodeURIComponent( baseSvg );
                }
                else {
                    return;
                }

                $(this)
                    .append( svg )
                    .css('background', 'none');
            });
        }, 100);
    });

    /**
     * https://github.com/davidchambers/Base64.js 1.0.0
     * @return void
     */
    function polyfill_atob() {
        !function(){function t(t){this.message=t}var r="undefined"!=typeof exports?exports:self,e="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";t.prototype=new Error,t.prototype.name="InvalidCharacterError",r.btoa||(r.btoa=function(r){for(var o,n,a=String(r),i=0,c=e,d="";a.charAt(0|i)||(c="=",i%1);d+=c.charAt(63&o>>8-i%1*8)){if(n=a.charCodeAt(i+=.75),n>255)throw new t("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.");o=o<<8|n}return d}),r.atob||(r.atob=function(r){var o=String(r).replace(/=+$/,"");if(o.length%4==1)throw new t("'atob' failed: The string to be decoded is not correctly encoded.");for(var n,a,i=0,c=0,d="";a=o.charAt(c++);~a&&(n=i%4?64*n+a:a,i++%4)?d+=String.fromCharCode(255&n>>(-2*i&6)):0)a=e.indexOf(a);return d})}();
    }
    </script>
    <?php
});

?>
